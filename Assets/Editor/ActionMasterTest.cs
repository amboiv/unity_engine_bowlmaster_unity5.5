﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class ActionMasterTest
{
	//General Arrange
	private ActionMasterScript _actionMaster;

	private ActionMasterScript.Action _endTurn = ActionMasterScript.Action.EndTurn;
	private ActionMasterScript.Action _tidy = ActionMasterScript.Action.Tidy;
	private ActionMasterScript.Action _reset = ActionMasterScript.Action.Reset;
	private ActionMasterScript.Action _endGame = ActionMasterScript.Action.EndGame;

	[SetUp]
	public void Setup ()
	{
		_actionMaster = new ActionMasterScript ();
	}

	[Test]
	public void EditorTest ()
	{
		//Arrange
		var gameObject = new GameObject ();

		//Act
		//Try to rename the GameObject
		var newGameObjectName = "My game object";
		gameObject.name = newGameObjectName;

		//Assert
		//The object has a new name
		Assert.AreEqual (newGameObjectName, gameObject.name);
	}

	[Test]
	public void T01OneStrikeReturnsEndTurn ()
	{
		//Arrange
		

		//Act

		//Assert
		Assert.AreEqual (_endTurn, _actionMaster.Bowl(10));
	}

	[Test]
	public void T02Bowl8ReturnsTidy ()
	{
		//Arrange

		//Act

		//Assert
		Assert.AreEqual (_tidy, _actionMaster.Bowl (8));
	}

	[Test]
	public void T03Bowl28ReturnsEndTurn ()
	{
		//Arrange
		

		//Act

		//Assert
		_actionMaster.Bowl (2);
		Assert.AreEqual (_endTurn, _actionMaster.Bowl (8));
	}

	[Test]
	public void T04CheckResetAtStrikeInLastFrame()
	{
		int[] rolls = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

		foreach (var roll in rolls)
		{
			_actionMaster.Bowl (roll);
		}
		Assert.AreEqual (_reset, _actionMaster.Bowl (10));
	}

	[Test]
	public void T05CheckResetAtSpareInLastFrame ()
	{
		int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

		foreach (var roll in rolls)
		{
			_actionMaster.Bowl (roll);
		}
		_actionMaster.Bowl (1);
		Assert.AreEqual (_reset, _actionMaster.Bowl (9));
	}

	[Test]
	public void T06YoutubeRollsEndInEndGame ()
	{
		//URL: https://www.youtube.com/watch?v=aBe71sD8o8c

		int[] rolls = { 8, 2, 7, 3, 3, 4, 10, 2, 8, 10, 10, 8, 0, 10, 8, 2};
		foreach (var roll in rolls)
		{
			_actionMaster.Bowl (roll);
		}
		Assert.AreEqual (_endGame, _actionMaster.Bowl (9));
	}

	[Test]
	public void T07GameEndsAtBowl20 ()
	{
		int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
		foreach (var roll in rolls)
		{
			_actionMaster.Bowl (roll);
		}
		Assert.AreEqual (_endGame, _actionMaster.Bowl (1));
	}

	[Test]
	public void T08TidyAtNoStrikeBowl20IfStrikeAtBowl19 ()
	{
		int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
		foreach (var roll in rolls)
		{
			_actionMaster.Bowl (roll);
		}
		_actionMaster.Bowl (10);
		Assert.AreEqual (_tidy, _actionMaster.Bowl (5));
	}

	[Test]
	public void T09TidyAtBowl20IsZeroIfStrikeAtBowl19 ()
	{
		int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
		foreach (var roll in rolls)
		{
			_actionMaster.Bowl (roll);
		}
		_actionMaster.Bowl (10);
		Assert.AreEqual (_tidy, _actionMaster.Bowl (0));
	}

	[Test]
	public void T10TidyAtZeroThenStrikeThen5ThenOne ()
	{
		int[] rolls = { 0, 10, 5 };
		foreach (var roll in rolls)
		{
			_actionMaster.Bowl (roll);
		}
		Assert.AreEqual (_endTurn, _actionMaster.Bowl (1));
	}

	[Test]
	public void T11DondisTripleStrikeLastFrame ()
	{
		int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
		foreach (var roll in rolls)
		{
			_actionMaster.Bowl (roll);
		}
		Assert.AreEqual (_reset, _actionMaster.Bowl (10));
		Assert.AreEqual (_reset, _actionMaster.Bowl (10));
		Assert.AreEqual (_endGame, _actionMaster.Bowl (10));
	}
}