﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinScript : MonoBehaviour
{
	public float StandingThresholdAngle = 10f;
	public float RaiseLowerDistance = 40f;
	// Use this for initialization
	void Start ()
	{
	}

	public bool IsStanding ()
	{
		var xEuler = transform.eulerAngles.x;
		var zEuler = transform.eulerAngles.z;

		xEuler = Mathf.Abs ((xEuler > 180) ? xEuler - 360 : xEuler);
		zEuler = Mathf.Abs ((zEuler > 180) ? zEuler - 360 : zEuler);

		return !(xEuler >= StandingThresholdAngle) &&
		       !(zEuler >= StandingThresholdAngle);
	}

	public void RaiseIfStanding ()
	{
		if (!IsStanding ()) return;
		//GetComponent <Rigidbody> ().useGravity = false;
		GetComponent<Rigidbody> ().isKinematic = true;
		transform.Translate (Vector3.up * RaiseLowerDistance, Space.World);
	}

	public void Lower ()
	{
		transform.Translate (Vector3.down * RaiseLowerDistance, Space.World);
		GetComponent<Rigidbody> ().isKinematic = false;
		//GetComponent<Rigidbody> ().useGravity = true;
	}
}