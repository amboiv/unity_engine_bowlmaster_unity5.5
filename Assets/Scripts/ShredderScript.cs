﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShredderScript : MonoBehaviour
{
	private void OnTriggerExit (Collider other)
	{
		if (other.GetComponentInParent<PinScript> () != null)
		{
			Destroy (other.transform.parent.gameObject);
		}
	}
}