﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionMasterScript
{
	public enum Action
	{
		Tidy,
		Reset,
		EndTurn,
		EndGame
	}

	private int[] _bowls = new int[21];
	private int _bowl = 1;

	public Action Bowl (int pins)
	{
		if (pins < 0 || pins > 10) {throw new UnityException("Wrong pin count!!");}

		_bowls[_bowl-1] = pins;

		if (_bowl == 21)
		{
			return Action.EndGame;
		}

		//Last frame special cases
		if (_bowl >= 19 && pins == 10)
		{
			_bowl++;
			return Action.Reset;
		}
		else if (_bowl == 20)
		{
			_bowl++;
			if (_bowls[19 - 1] == 10 && _bowls[20 - 1] != 10)
			{
				return Action.Tidy;
			}
			if (DoesBowl19Plus20Sum10Or20 ())
			{
				return Action.Reset;
			}
			return IsBowl21Awarded () ? Action.Tidy : Action.EndGame;
		}

		if (_bowl % 2 != 0) //We are in first bowl of frame
		{
			if (pins == 10)
			{
				_bowl += 2;
				return Action.EndTurn;
			}
			_bowl++;
			return Action.Tidy;
		}
		else if (_bowl % 2 == 0)	//Second bowl of frame
		{
			_bowl++;
			return Action.EndTurn;
		}
		
		throw new UnityException("Not sure what action to return!!");
	}

	private bool IsBowl21Awarded ()
	{
		return (_bowls[19-1] + _bowls[20-1] >= 10);
	}

	private bool DoesBowl19Plus20Sum10Or20 ()
	{
		return ((_bowls[19 - 1] + _bowls[20 - 1]) % 10 == 0);
	}
}