﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{
	public Vector3 LaunchVelocity;
	public Vector3 MaxVelocity;
	public bool IsInPlay = false;

	private Rigidbody _rigidbody;
	private AudioSource _ballAudioSource;
	private Vector3 _ballStartPosition;
	// Use this for initialization
	void Start ()
	{
		_ballStartPosition = transform.position;
		_ballAudioSource = GetComponent<AudioSource> ();
		_rigidbody = GetComponent <Rigidbody> ();
		_rigidbody.useGravity = false;

		//Launch (LaunchVelocity);
	}

	public void Launch (Vector3 velocity)
	{
		IsInPlay = true;
		_rigidbody.useGravity = true;

		//Clamping speeds in x and z direction
		_rigidbody.velocity = new Vector3(Mathf.Clamp(velocity.x, -MaxVelocity.x, MaxVelocity.x), velocity.y, Mathf.Clamp (velocity.z, 0, MaxVelocity.z));
		_ballAudioSource.Play ();
	}

	public void Reset ()
	{
		Debug.Log ("Resetting Ball");
		transform.position = _ballStartPosition;
		_rigidbody.velocity = Vector3.zero;
		_rigidbody.angularVelocity = Vector3.zero;
		_rigidbody.useGravity = false;
		IsInPlay = false;
	}
}