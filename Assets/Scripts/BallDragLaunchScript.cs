﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BallScript))]
public class BallDragLaunchScript : MonoBehaviour
{
	private BallScript _ballScript;
	private Vector3 _dragStart, _dragEnd;
	private float _startTime, _endTime;
	// Use this for initialization
	void Start ()
	{
		_ballScript = GetComponent <BallScript> ();
	}

	public void DragStart ()
	{
		_dragStart = Input.mousePosition;
		_startTime = Time.time;
	}

	public void DragEnd ()
	{
		_dragEnd = Input.mousePosition;
		_endTime = Time.time;

		var deltaPosition = _dragEnd - _dragStart;
		var dragDeltaTime = _endTime - _startTime;

		var launchVelocity = new Vector3 (deltaPosition.x / dragDeltaTime, 0, deltaPosition.y / dragDeltaTime);

		_ballScript.Launch (launchVelocity);
	}

	public void MoveStart (float amount)
	{
		if (!_ballScript.IsInPlay)
		{
			_ballScript.transform.Translate (new Vector3 (amount, 0, 0));
		}

	}
}