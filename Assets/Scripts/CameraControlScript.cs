﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControlScript : MonoBehaviour
{
	public BallScript Ball;
	public float CamStopPosition = 1800;

	private Vector3 _offset;
	
	// Use this for initialization
	void Start ()
	{
		_offset = (Ball.transform.position - transform.position);
	}

	// Update is called once per frame
	void Update ()
	{
		if (Ball.transform.position.z < CamStopPosition)
		{
			FollowBall ();
		}
	}

	private void FollowBall ()
	{
		transform.position = Ball.transform.position - _offset;
	}
}