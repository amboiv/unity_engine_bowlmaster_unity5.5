﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PinSetterScript : MonoBehaviour
{
	public Text StandingDisplay;
	public int LastStandingCount = 10;
	public GameObject PinSet;
	public const float SettleTimeS = 4f;

	private BallScript _ball;
	private float _lastChangeTime;
	private PinScript[] _pins;
	private bool _ballEnteredBox;
	private ActionMasterScript _actionMaster = new ActionMasterScript ();
	private Animator _animator;

	// Use this for initialization
	void Start ()
	{
		_pins = FindObjectsOfType <PinScript> ();
		_ball = FindObjectOfType <BallScript> ();
		_animator = GetComponent <Animator> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!_ballEnteredBox) return;

		UpdateStandingCountAndSettle ();	
		StandingDisplay.text = CountStanding ().ToString ();
	}

	public void RaisePins ()
	{
		//GameObject.Find ("BowlingPins").transform.position = Vector3.up * DistanceToRaise;
		foreach (PinScript pin in FindObjectsOfType<PinScript> ())
		{
			pin.RaiseIfStanding ();
		}
	}

	public void LowerPins ()
	{
		foreach (PinScript pin in FindObjectsOfType<PinScript>())
		{
			pin.Lower ();
		}
	}

	public void RenewPins ()
	{
		GameObject newPins = Instantiate (PinSet);
		newPins.transform.position += new Vector3(0, 30, 0);
	}

	private void UpdateStandingCountAndSettle ()
	{
		var currentStanding = CountStanding ();

		if (currentStanding != LastStandingCount)
		{
			_lastChangeTime = Time.time;
			LastStandingCount = currentStanding;
			return;
		}

		
		if (Time.time - _lastChangeTime > SettleTimeS)
		{
			PinsHaveSettled ();
		}
	}

	private void PinsHaveSettled ()
	{
		var pinFall = 10 - LastStandingCount;

		var action = _actionMaster.Bowl (pinFall);
		Debug.Log ("pinFall = " + pinFall + "; Action = " + action);

		switch (action)
		{
			case ActionMasterScript.Action.Tidy:
				_animator.SetTrigger ("TidyTrigger");
				break;
			case ActionMasterScript.Action.EndTurn:
				_animator.SetTrigger ("ResetTrigger");
				break;
			case ActionMasterScript.Action.Reset:
				_animator.SetTrigger ("ResetTrigger");
				break;
			case ActionMasterScript.Action.EndGame:
				throw new UnityException("Don't know how to handle EndGame yet");
			default:
				throw new UnityException ("No way to handle enum value: " + action);
		}


		LastStandingCount = 10;
		_ballEnteredBox = false;
		StandingDisplay.color = Color.green;
		_ball.Reset ();
	}

	private int CountStanding ()
	{
		_pins = FindObjectsOfType <PinScript> ();

		return _pins.Count (pin => pin.IsStanding ());
	}

	private void OnTriggerEnter (Collider other)
	{
		if (!other.GetComponent <BallScript> ()) return;
		StandingDisplay.color = Color.red;
		_ballEnteredBox = true;
	}
}
